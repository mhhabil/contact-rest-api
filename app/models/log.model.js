class Log {
    constructor(sequelize, Sequelize) {
        this.sequelize = sequelize;
        this.Sequelize = Sequelize;
    }
    getLog() {
        const Log = this.sequelize.define("logs", {
            log_id:{
                type: this.Sequelize.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            contact_id:{
                type: this.Sequelize.INTEGER,

            },
            // waktu_panggilan:{
            //     type: this.Sequelize.DATE,
            //     allowNull: true,
            // }
        
        })
        return Log;
    }
}
module.exports = Log;

// SELECT C.nama, C.no_hp, C.email 
// FROM `logs` L JOIN contacts C ON C.contact_id = L.contact_id
// SELECT *
// FROM `logs` L
// LEFT JOIN contacts C
// ON C.contact_id = L.contact_id

// SELECT *
// FROM `logs` L
// RIGHT JOIN contacts C
// ON C.contact_id = L.contact_id