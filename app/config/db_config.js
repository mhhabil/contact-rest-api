const secureEnv = require('secure-env');
global.env = secureEnv({secret: 'gicsmecsecret'});

const mysql = {
    HOST: global.env.DB_HOST,
    USER: global.env.DB_USER,
    PASSWORD: global.env.DB_PASS,
    DB: global.env.DB_NAME,
    dialect: 'mysql',
}

const redis = {
    port: global.env.REDIS_PORT, // Redis port
    host: global.env.REDIS_HOST, // Redis host
    family: global.env.REDIS_FAMILY, // 4 (IPv4) or 6 (IPv6)
    db: global.env.REDIS_DB,
}

module.exports =  {mysql, redis} ;
