const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")

//routes
const routesIndex = require('./routes/index.routes');
// Models
const db = require("./models");


const app = express();

let whiteList = [
    'http://localhost:7070'
];

let corsOption = {
    origin: function (origin, callback) {
        if (whiteList.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

app.use(cors(corsOption));

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({ extended: true}))

db.sequelize.sync();

app.get("/", (req, res) => {
    res.json({
        message: "I am Muhammad Habil"
    });
});

//routes
app.use("/api/contacts", routesIndex);


const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`)
});